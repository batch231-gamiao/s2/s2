//create student one
// let studentOneName = 'Tony';
// let studentOneEmail = 'starksindustries@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Peter';
// let studentTwoEmail = 'spideyman@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Wanda';
// let studentThreeEmail = 'scarlettMaximoff@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Steve';
// let studentFourEmail = 'captainRogers@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

// encapsulation - organize related information (properties) and behavior (methods) to belong to a single entity.

// Encapsulate the following information into 4 student objects using object literals
let studentOne = {
	name: 'Tony',
	email: 'starksindustries@mail.com',
	grades: [89,84,78,88],

	// add the functionalities available to a student as objects methods
		// keyword "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(email){
    	console.log(`${this.email} has logged out`);
 	},
 	listGrades(grades){
 	    this.grades.forEach(grade => {
 	        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
 	    })
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach(grade=> sum = sum + grade);
		return sum/4;
	},

	// mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise.
	// name the method willPass
	willPass() {
		return (this.computeAve() >= 85) ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	} 

}
console.log(`student one's name is ${studentOne.name}`);
console.log(this); // result: global window object

let studentTwo = {
	name: 'Peter',
	email: 'spideyman@mail.com',
	grades: [78,82,79,85],

	// add the functionalities available to a student as objects methods
		// keyword "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(email){
    	console.log(`${this.email} has logged out`);
 	},
 	listGrades(grades){
 	    this.grades.forEach(grade => {
 	        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
 	    })
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach(grade=> sum = sum + grade);
		return sum/4;
	},

	// mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise.
	// name the method willPass
	willPass() {
		return (this.computeAve() >= 85) ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	} 

}

let studentThree = {
	name: 'Wanda',
	email: 'scarlettMaximoff@mail.com',
	grades: [87, 89, 91, 93],

	// add the functionalities available to a student as objects methods
		// keyword "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(email){
    	console.log(`${this.email} has logged out`);
 	},
 	listGrades(grades){
 	    this.grades.forEach(grade => {
 	        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
 	    })
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach(grade=> sum = sum + grade);
		return sum/4;
	},

	// mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise.
	// name the method willPass
	willPass() {
		return (this.computeAve() >= 85) ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	} 

}

let studentFour = {
	name: 'Steve',
	email: 'captainRogers@mail.com',
	grades: [91, 89, 92, 93],

	// add the functionalities available to a student as objects methods
		// keyword "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(email){
    	console.log(`${this.email} has logged out`);
 	},
 	listGrades(grades){
 	    this.grades.forEach(grade => {
 	        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
 	    })
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach(grade=> sum = sum + grade);
		return sum/4;
	},

	// mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise.
	// name the method willPass
	willPass() {
		return (this.computeAve() >= 85) ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	} 

}

// Mini-Quiz:
/*
	   1. What is the term given to unorganized code that's very hard to work with?

	   - spaghetti code
	   

	   2. How are object literals written in JS?

	   - {}

	   3. What do you call the concept of organizing information and functionality to belong to an object?

	   - encapsulation
	   
	   4. If student1 has a method named enroll(), how would you invoke it?

	   - studentOne.enroll();
	 

	   5. True or False: Objects can have objects as properties.

	   - true
	 

	   6. What does the this keyword refer to if used in an arrow function method?

	   - global window object
	   

	   7. True or False: A method can have no parameters and still work.

	   - true

	   8. True or False: Arrays can have objects as elements.
	   
	   - true

	   9. True or False: Arrays are objects.

	   - true
	   

	   10. True or False: Objects can have arrays as properties. 

	   - true
*/

const classOf1A = {
	students: [studentOne,studentTwo, studentThree, studentFour],
	countHonorStudents(){
		let result = 0;
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				result++;
			}
		})
		return result;
	},
	// Activity Solution
	// 1.
	honorsPercentage(){
		let numOfStudent = this.students.length;
		let numOfHonors = this.countHonorStudents();

		return (100 * numOfHonors) / numOfStudent;
	},

	// 2. 
	retrieveHonorStudentInfo(){		
		let createNewArr = [];
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				return createNewArr.push({email: student.email, average: student.computeAve()})
			}
		})

		return createNewArr;
	},

	// 3. 
	sortHonorStudentsByGradeDesc(){
		let newArr = this.retrieveHonorStudentInfo();
		newArr.sort((a,b)=> b.average - a.average);	

		return newArr;
	}
}
